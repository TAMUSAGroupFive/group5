import javax.swing.JOptionPane;

/**
 * 
 */

/**
 * @author avill
 *
 */
public class FoodCategories {
	//Choose A Category
	
	public static void Category () {
		System.out.println("The Category options are: ");
		System.out.println("1: Slow Cook");
		System.out.println("2: Pizza");
		System.out.println("3: Pork");
		System.out.println("4: Vegan");
		System.out.println("5: Desserts");
		System.out.println("6: Salads & Vegetables");
		System.out.println("7: Vegetarian");
		System.out.println("8: Seafood");
		System.out.println("9: Gluten Free");
		System.out.println("10: Pasta");
		System.out.println("11: Burgers & Sandwiches");
		System.out.println("12: Chicken & Turkey");
		System.out.println("13: Soup");
		System.out.println("14: Beef");
		System.out.println("15: Quick & Easy");
		System.out.println("16: Appetizers");
		System.out.println("17: Breakfast");
		System.out.println("18: Brunch");
		System.out.println("19: Lunch");
		System.out.println("20: Dinner");
		
		int category = Integer.parseInt(JOptionPane.showInputDialog("Please choose a category"));
		
		switch(category) { 
		case 1 :
			JOptionPane.showMessageDialog(null,"Slow Cook : ");
			JOptionPane.showMessageDialog(null, "Recipes for when you know your not gonna want to cook later.");
			break;
		case 2 :
			JOptionPane.showMessageDialog(null,"Pizza : ");
			JOptionPane.showMessageDialog(null, "This category has the best pizza recipes for game night!");
			break;
		case 3 :
			JOptionPane.showMessageDialog(null,"Pork : ");
			JOptionPane.showMessageDialog(null, "This category answeres the question: whats shaking bacon? ");
			break;
		case 4 :
			JOptionPane.showMessageDialog(null,"Vegan : ");
			JOptionPane.showMessageDialog(null, "Awesome recipes that don't use any animal products.");
			break;
		case 5 :
			JOptionPane.showMessageDialog(null,"Desserts : ");
			JOptionPane.showMessageDialog(null, "Satisfy your sweet tooth with these recipes.");
			break;
		case 6 :
			JOptionPane.showMessageDialog(null,"Salads & Vegtables : ");
			JOptionPane.showMessageDialog(null, "The recipes in this categories were designed for you to veg out.");
			break;
		case 7 :
			JOptionPane.showMessageDialog(null,"Vegetarian : ");
			JOptionPane.showMessageDialog(null, "Recipes for those that love animals but also cheese...");
			break;
		case 8 :
			JOptionPane.showMessageDialog(null,"Seafood : ");
			JOptionPane.showMessageDialog(null, "The only thing fishy about these recipes are the awesome ingredients!");
			break;
		case 9 :
			JOptionPane.showMessageDialog(null,"Gluten Free : ");
			JOptionPane.showMessageDialog(null, "Recipes that go against the grain.");
			break;
		case 10 :
			JOptionPane.showMessageDialog(null,"Pasta : ");
			JOptionPane.showMessageDialog(null, "These recipes took you seriously when you said send noods.");
			break;
		case 11 :
			JOptionPane.showMessageDialog(null,"Burgers & Sandwhiches : ");
			JOptionPane.showMessageDialog(null, "This category is packing all the right meats.");
			break;
		case 12 :
			JOptionPane.showMessageDialog(null,"Chicken & Turkey : ");
			JOptionPane.showMessageDialog(null, "You gonna gobble down the cluckin recipes.");
			break;
		case 13 :
			JOptionPane.showMessageDialog(null,"Soups : ");
			JOptionPane.showMessageDialog(null, "Recipes for when ramen just inst enough.");
			break;
		case 14 :
			JOptionPane.showMessageDialog(null,"Beef : ");
			JOptionPane.showMessageDialog(null, "A fight is about to break out in here cuz all these recipes got beef.");
			break;
		case 15 :
			JOptionPane.showMessageDialog(null,"Quick & Easy : ");
			JOptionPane.showMessageDialog(null, "All the fun of cooking minus the effort.");
			break;
		case 16 :
			JOptionPane.showMessageDialog(null,"Appetizers : ");
			JOptionPane.showMessageDialog(null, "Great recipes for a meal before your real meal.");
			break;
		case 17 :
			JOptionPane.showMessageDialog(null,"Breakfast : ");
			JOptionPane.showMessageDialog(null, "Morning time recipes that won't go bacon your heart.");
			break;
		case 18 :
			JOptionPane.showMessageDialog(null,"Brunch : ");
			JOptionPane.showMessageDialog(null, "Mimosas? Yes but also food.");
			break;
		case 19 :
			JOptionPane.showMessageDialog(null,"Lunch : ");
			JOptionPane.showMessageDialog(null, "Recipes that just have mid-day vibes.");
			break;
		case 20:
			JOptionPane.showMessageDialog(null,"Dinner : ");
			JOptionPane.showMessageDialog(null, "These recipes will satisfy your night time nom noms.");
			break;
	}
		JOptionPane.showMessageDialog(null,"Filtering Recipes ... ");
	}
}
